package com.beehyv.learnflix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;


@SpringBootApplication
public class LearnflixApplication {

    public static void doCrypt(String sourceFilePath, String destFilePath, int mode) {
        try {
            Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");

            byte[] secretKey = Base64.getDecoder().decode("uQsaW+WMUrjcsq1HMf+2JQ==");

            SecretKeySpec key = new SecretKeySpec(secretKey, "AES");
            IvParameterSpec iv = new IvParameterSpec(secretKey);

            cipher.init(mode, key , iv);

            FileInputStream fileInputStream = new FileInputStream(sourceFilePath);
            FileOutputStream fileOutputStream = new FileOutputStream(new File(destFilePath));

            int read = 0;
            while ((fileInputStream.available()) > 0) {
                byte[] block = new byte[4096];
                read = fileInputStream.read(block);
                byte[] writeBuffer = cipher.update(block);
                fileOutputStream.write(writeBuffer, 0, read);
            }

            byte[] writeBuffer = cipher.doFinal();
            fileOutputStream.write(writeBuffer, 0, writeBuffer.length);

            fileInputStream.close();
            fileOutputStream.close();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        SpringApplication.run(LearnflixApplication.class, args);
//        doCrypt("tce_ignitor.html", "abc.html", 1); //encrypt mode
//        doCrypt("abc.html", "asdf.html",2); //decrypt mode
    }

}


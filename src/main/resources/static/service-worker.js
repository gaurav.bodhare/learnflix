self.importScripts(
    "CryptoJS/components/core.js",
    "CryptoJS/rollups/aes.js",
    "CryptoJS/components/cipher-core.js",
    "CryptoJS/components/enc-base64.js",
    "CryptoJS/components/enc-utf16.js",
    "CryptoJS/components/mode-ctr.js",
    "CryptoJS/components/pad-nopadding.js");

self.addEventListener('install', function (event) {
    event.waitUntil(self.skipWaiting());
});

self.addEventListener('activate', function (event) {
    event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', function (event) {
    if (contentEncryption(event.request.url)) {
        event.respondWith(
            fetch(event.request).then(function (response) {
                var init = {
                    status: response.status,
                    statusText: response.statusText,
                    headers: {}
                };

                response.headers.forEach(function (v, k) {
                    init.headers[k] = v;
                });
                var ext  = event.request.url.split('.').pop().toLowerCase();
                var isMedia = (ext !== 'html') && (ext !== 'js');
                return response.arrayBuffer().then(function (buffer) {
                    return new Response(decryptFile(buffer, isMedia), init);
                });
            })
        );
    } else if (event.request.url.split('/').pop() === 'enc_keys') {
        event.respondWith(
            fetch(event.request).then(function (response) {
                var init = {
                    status: response.status,
                    statusText: response.statusText,
                    headers: {}
                };

                response.headers.forEach(function (v, k) {
                    init.headers[k] = v;
                });
                return response.text().then(function (text) {

                    return new Response("", init);
                });
            })
        );
    }
});

function contentEncryption(filepath) {
    if (filepath.indexOf('encrypted') > -1) {
        var ext = filepath.split('.').pop().toLowerCase();
        return (ext === 'html') || (ext === 'js');
    }
    return false;
}

function decryptFile(encryptedContent, isMedia) {

    var encryptedHex = buf2hex(encryptedContent);

    var key = CryptoJS.enc.Base64.parse('uQsaW+WMUrjcsq1HMf+2JQ==');

    var cipherParams = CryptoJS.lib.CipherParams.create({
        ciphertext: CryptoJS.enc.Hex.parse(encryptedHex)
    });

    var decrypted = CryptoJS.AES.decrypt(cipherParams, key, {
        mode: CryptoJS.mode.CTR,
        iv: key,
        padding: CryptoJS.pad.NoPadding
    });

    var decryptedText;

    if (isMedia) {
        decryptedText = CryptoJS.enc.Base64.stringify(decrypted);
        return b64toBlob(decryptedText);
    } else {
        decryptedText = CryptoJS.enc.Utf8.stringify(decrypted);
    }

    console.log( "Decrypted Content : " + decryptedText);

    return decryptedText;
}

function buf2hex(buffer) {
    var byteArray = new Uint8Array(buffer);
    var hexParts = [];
    for(var i = 0; i < byteArray.length; i++) {
        var hex = byteArray[i].toString(16);
        var paddedHex = ('00' + hex).slice(-2);
        hexParts.push(paddedHex);
    }
    return hexParts.join('');
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

